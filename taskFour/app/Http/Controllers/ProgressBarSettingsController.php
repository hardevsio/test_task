<?php

namespace App\Http\Controllers;

use App\ProgressBarSettings;
use Illuminate\Http\Request;
use Mockery\Exception;

class ProgressBarSettingsController extends Controller
{
    private $data_model = null;
    private $limit = 2;

    /**
     * ProgressBarSettingsController constructor. Init data_model.
     */
    public function __construct()
    {
        $this->data_model = new ProgressBarSettings();
    }

    /**
     * The function run, when starting main page.
     */
    public function generalSettings()
    {
        try {
            $title = 'General Settings';

            $metafields = $this->data_model->getShopMetaFields([
                'fields' => 'key,value',
                'namespace' => 'progress_bar'
            ]);
            $arr = [];
            foreach ($metafields as $mf)
                $arr[$mf['key']] = $mf['value'];
            $metafields = $arr;

            $btn = [ "href" => "/products", "title" => "Products" ];

            return view('default-settings', compact('title', 'metafields', 'btn' ));
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     *  The function run when starting product page.
     * @param $id - id product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string - view page default-settings or error string
     */
    public function productSettings($id)
    {
        try {
            $title = 'Product Settings';

            $metafields = $this->data_model->getProductMetaFields($id,[
                'fields' => 'key,value',
                'namespace' => 'progress_bar'
            ]);
            $arr = [];
            foreach ($metafields as $mf)
                $arr[$mf['key']] = $mf['value'];
            $metafields = $arr;

            $btn = [ "href" => "./default-settings", "title" => "Default settings" ];

            $back = [ "href" => "/products", "title" => "< Products" ];

            return view('default-settings', compact('title', 'metafields', 'btn', 'back', 'id' ));
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * The function run when starting products page.
     * @param $request - get and post data
     * @param $page - page with list products
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string - view page products or error string
     */
    public function products(Request $request, $page = 1)
    {
        try {
            if( $request->method() == 'GET' && $request->path() == 'products')
                session()->remove('title');
            $title = 'All Products';

            $params = [
                'limit' => $this->limit,
                'page' => $page,
                'fields' => 'id,image,title,body_html'
            ];
            if( isset( $_POST['title'] ) )
                session(['title' => $params['title'] = $_POST['title']]);
            else if( session()->has('title') )
                $params['title'] = session('title');
            $products = $this->data_model->getProducts($params);
            $count = $this->data_model->getProductsCount($params);
            $search = isset($params['title']) ? $params['title'] : '';
            $limit = $params['limit'];
            $back = [ "href" => "/", "title" => "< General Settings" ];

            return view('products', compact('title', 'products', 'count', 'search', 'page', 'limit', 'back'));
        } catch ( Exception $ex ) {
            return $ex->getMessage();
        }
    }

    /**
     * The function run when post request /save .
     * @param $request - get and post data
     * @return \Illuminate\Http\JsonResponse message = ok or error
     */
    public function saveSettings(Request $request)
    {
        try {

            $this->validate($request, [
                'green' => 'bail|required|integer',
                'red' => 'bail|required|integer',
            ]);

            $params = [ 'green' => [
                'key' => 'count_product_green',
                'value' => $request->post('green'),
                'value_type' => 'integer',
                'namespace' => 'progress_bar'
            ], 'red' => [
                'key' => 'count_product_red',
                'value' => $request->post('red'),
                'value_type' => 'integer',
                'namespace' => 'progress_bar'
            ], 'quantity' => [
                'key' => 'outputQuantity',
                'value' => $request->post('quantity') == 'true' ? 1 : 0,
                'value_type' => 'integer',
                'namespace' => 'progress_bar'
            ] ];

            foreach ($params as $param)
                if($request->has('id'))
                    $this->data_model->postProductMetaFields($request->post('id'), $param);
                else
                    $this->data_model->postShopMetaFields($param);


            return response()->json(['message' => 'Saved is good!']);
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    /**
     * The function run, when admin clicked on Default Settings on page product.
     * @param $id - product id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse - error message or refresh page
     */
    public function deleteSettings($id)
    {
        try {
            $metafields = $this->data_model->getProductMetaFields($id,[
                'fields' => 'id',
                'namespace' => 'progress_bar'
            ]);
            foreach ($metafields as $mf)
                $this->data_model->deleteProductMetaFields($id, $mf['id']);

            return response()->redirectTo('/product/' . $id . '/');
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }
}
