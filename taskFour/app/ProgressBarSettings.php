<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PHPShopify\ShopifySDK;

class ProgressBarSettings extends Model
{
    private $shopify;

    public function __construct()
    {
        $this->shopify = ShopifySDK::config([
            'ShopUrl' => env('SHOPIFY_DOMAIN'),
            'ApiKey' => env('SHOPIFY_KEY'),
            'Password' => env('SHOPIFY_PASSWORD'),
        ]);

        ShopifySDK::checkApiCallLimit();
    }

    public function getProducts(array $data = [])
    {
        return $this->shopify->Product->get($data);
    }

    public function getProductsCount(array $data = [])
    {
        return $this->shopify->Product->count($data);
    }

    public function getShopMetaFields(array $data = [])
    {
        return $this->shopify->Metafield->get($data);
    }

    public function getProductMetaFields($id, array $data = [])
    {
        return $this->shopify->Product($id)->Metafield->get($data);
    }

    public function postShopMetaFields(array $data)
    {
        return $this->shopify->Metafield->post($data);
    }

    public function postProductMetaFields($id, array $data)
    {
        return $this->shopify->Product($id)->Metafield->post($data);
    }

    public function deleteProductMetaFields($id, $mf_id)
    {
        return $this->shopify->Product($id)->Metafield($mf_id)->delete();
    }
}
