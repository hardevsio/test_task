<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProgressBarSettingsController@generalSettings');

Route::post('/save', 'ProgressBarSettingsController@saveSettings');

Route::match(['get', 'post'], '/products', 'ProgressBarSettingsController@products');

Route::get('/products/{page}', 'ProgressBarSettingsController@products');

Route::get('/product/{product}/', 'ProgressBarSettingsController@productSettings');

Route::get('/product/{product}/default-settings', 'ProgressBarSettingsController@deleteSettings');
