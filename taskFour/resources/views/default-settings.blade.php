<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <title><?=$title?></title>
</head>
<body class="bg-light">
    <div class="container">
        <h1><?=$title?></h1>
        <?=isset($back)?'<hr /><a class="btn btn-link" href="' . $back['href'] . '">' . $back['title'] . '</a>':''?>
        <hr />
        <div class="container">
            <div id="message" class="row">

            </div>
            <div class="row">
                <div class="col-sm">
                    <h4>Main settings</h4>
                    <p>ProgressBar display settings</p>
                </div>
                <div class="col-sm">
                    <form id="settings" class="bg-white p-4 rounded">
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="green">The number of products in the stock, symbolizing a high level of availability (green bar progress)</label>
                                <input type="number" min="0" class="form-control" id="green" required value="<?=isset($metafields['count_product_green'])?$metafields['count_product_green']:''?>" >
                            </div>
                            <div class="form-group col-md">
                                <label for="red">The number of products in the stock, symbolizing a low level of availability (red bar progress)</label>
                                <input type="number" min="0" class="form-control" id="red" required value="<?=isset($metafields['count_product_red'])?$metafields['count_product_red']:''?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-auto my-1">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input cursor-pointer" id="quantity" <?=isset($metafields['outputQuantity']) && $metafields['outputQuantity']?'checked':''?>>
                                    <label class="custom-control-label cursor-pointer" for="quantity"><b>output / do not output</b> actual quantity in stock</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <hr />
        <div class="d-flex justify-content-between">
            <a href="<?=$btn['href']?>" id="btn-default-settings" class="btn btn-secondary mt-2"><?=$btn['title']?></a>
            <a href="/save" id="btn-save" class="btn btn-primary mt-2">Save</a>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        $(function () {
            $('#btn-save').click(function (e) {
                e.preventDefault();
                $('#message').html('');
                $.post($(this).attr('href'), { green: $('#green').val(), red: $('#red').val(), quantity: $('#quantity')[0].checked<?= isset($id) ? ', id: ' . $id : '' ?> } , function (data) {
                    if( data['error'] === undefined )
                        $('#message').append('<div class="alert alert-success" role="alert">' + data['message'] + '</div>');
                    else
                        $('#message').append('<div class="alert alert-danger" role="alert">' + data['error'] + '</div>');
                }).fail(function(error) {
                    var message = error.responseJSON.message;
                    if(error.responseJSON.errors.green != undefined)
                        message += '<br />' + error.responseJSON.errors.green[0];
                    if(error.responseJSON.errors.red != undefined)
                        message += '<br />' + error.responseJSON.errors.red[0];
                    $('#message').append('<div class="alert alert-danger" role="alert">' +  message  + '</div>');
                });
            });
        });
    </script>
</body>
</html>
