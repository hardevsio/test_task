<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <title><?=$title?></title>
    <style>
        .cursor-pointer{
            cursor: pointer;
        }
    </style>
</head>
<body class="bg-light">
<div class="container">
    <h1><?=$title?></h1>
    <?=isset($back)?'<hr /><a class="btn btn-link" href="' . $back['href'] . '">' . $back['title'] . '</a>':''?>
    <hr />
    <div class="container">
        <div id="message" class="row">

        </div>
        <div class="row bg-white p-4 rounded">
            <div class="input-group mb-3">
                <input id="input-search" type="text" value="<?=$search?>" class="form-control" placeholder="Search products" aria-label="Search products" aria-describedby="btn-search">
                <div class="input-group-append">
                    <button class="btn btn-outline-dark" type="button" id="btn-search">Search</button>
                </div>
            </div>
            <table id="products" class="table table-hover">
                <tbody>
                    @foreach( $products as $product )
                    <tr class="cursor-pointer" data-id="<?=$product['id']?>">
                        <th scope="row" style="width: 150px"><img width="100px" src="<?=$product['image']['src']?$product['image']['src']:'/img/notfound.png'?>" alt="<?=$product['title']?>"></th>
                        <td>
                            <h3><?=$product['title']?></h3>
                            <p><?=mb_strimwidth($product['body_html'], 0, 250, '...')?></p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <nav aria-label="..." id="pagination" class="d-flex">
                <ul class="pagination pagination-lg mx-auto">
                    @for ( $i = 1; $i <= ceil($count / $limit); $i++)
                        <li class="page-item <?= $page == $i ? 'disabled' : ''; ?>"><a class="page-link" href="/products/<?=$i?>"><?=$i?></a></li>
                    @endfor
                </ul>
            </nav>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    $('#products tr').click(function (e) {
        e.preventDefault();
        window.location.href = '/product/' + $(this).attr('data-id') + '/';
    });
    $('#btn-search').click(function (e) {
        e.preventDefault();
        $.post('/products', { title: $('#input-search').val() } ,function (data) {
            window.location.href = '/products/1';
        });
    });
</script>
</body>
</html>