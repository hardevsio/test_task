<?php
namespace WorkWithStatistics;
use Dotenv\Dotenv;
use PHPShopify\ShopifySDK;

class Statistic {
    private $shopify = null; // Object ShopifySDK

    // Work with data
    private $products = []; // Data products
    private $productParams = []; // product params

    private $collections = []; // Data collections

    private $orders = []; // Data orders

    // Initialize params for Statistics template
    public $dates = [];
    public $types = [];
    public $categories = [];
    public $titles = [];
    public $statistics = [];

    // local params for thisObject
    private $title = null;

    /**
     * Statistics constructor. The constructor settings "ShopifySDK" and call initialize function.
     * @param $argsInit - params for initialize
     */
    public function __construct($argsInit) {
        $dotenv = new Dotenv(__DIR__ . str_repeat(DIRECTORY_SEPARATOR . '..', 2));
        $dotenv->load();

        $config = [
            'ShopUrl' => $_ENV['SHOP'],
            'ApiKey' => $_ENV['API_KEY'],
            'Password' => $_ENV['PASS'],
        ];

        $this->shopify = ShopifySDK::config($config);
        ShopifySDK::checkApiCallLimit();
        $this->initialize($argsInit);
    }

    /**
     * The function initialize this is object at make and after settings "ShopifySDK".
     * @param $args - params for initialize
     */
    private function initialize($args) {

        // collections init
        $this->initCollections( [
            'page' => 1,
            'limit' => 250,
            'fields' => 'id,title',
        ]);

        // orders init
        $this->initOrders( [
            'page' => 1,
            'limit' => 250,
            'status' => 'closed',
            'financial_status' => 'paid,refunded',
            'fields' => 'updated_at,line_items,financial_status',
        ], $args);

        // products init
        $this->initProducts( [
            'page' => 1,
            'limit' => 250,
            'fields' => 'id,product_type',
        ], $args);
    }

    /**
     * The function initialize collections array.
     * @param $params - params for get collections array
     */
    private function initCollections($params){
        do {
            $collectionsPage = $this->shopify->CustomCollection->get($params);
            if (!empty($collectionsPage))
                $this->collections = array_merge($this->collections, $collectionsPage);
            $params['page']++;
        } while (!empty($collectionsPage));
    }

    /**
     * The function initialize orders array.
     * @param $params - params for get orders array
     * @param $args - params for filter data
     */
    private function initOrders($params, $args){
        if (count($args)) {
            $params['updated_at_min'] = $args['min-date'];
            $params['updated_at_max'] = $args['max-date'];
        }
        do {
            $ordersPage = $this->shopify->Order->get($params);
            if (!empty($ordersPage))
                $this->orders = array_merge($this->orders, $ordersPage);
            $params['page']++;
        } while (!empty($ordersPage));
    }

    /**
     * The function initialize products array.
     * @param $params - params for get products array
     * @param $args - params for filter data
     */
    private function initProducts($params, $args){
        if ( count($args) ) {
            if ( $args['cat'] != 'no-selected' )
                $params['collection_id'] = $this->productParams['collection_id'] = $args['cat'];
            if ( $args['type'] != 'no-selected' )
                $params['product_type'] = $this->productParams['product_type'] = $args['type'];
            $this->title = $args['title'] == 'no-selected' ? null : $args['title'];
        }
        do {
            $productsPage = $this->shopify->Product->get($params);
            if (!empty($productsPage))
                $this->products = array_merge($this->products, $productsPage);
            $params['page']++;
        } while ( !empty($productsPage) );
    }

    /**
     * The function returned product information.
     * @param $productId - product id
     * @return mixed - product information | null
     */
    private function getProduct( $productId ) {
        foreach ( $this->products as $item )
            if( $productId == $item['id'] )
                return $item;
        return null;
    }

    /**
     * The function add categories in public variable from collections.
     * @param $collection - collection with products
     */
    private function collectCategories ( $collection ) {
        $this->categories[$collection['id']] = [
            'title' => $collection['title'],
            'selected' => $this->productParams['collection_id'] && $collection['id'] == $this->productParams['collection_id'],
        ];
    }

    /**
     * The function collect titles.
     * @param $name - title name
     */
    private function collectTitles ( $name ){
        $this->titles[$name] = [
            'title' => $name,
            'selected' => $this->title && $name == $this->title,
        ];
    }

    /**
     * The function collect types.
     * @param $type - type name
     */
    private function collectTypes ( $type ){
        $this->types[$type] = [
            'title' => $type,
            'selected' => $this->productParams['product_type'] && $this->productParams['product_type'] == $type,
        ];
    }

    /**
     *  The function collection statistics and titles and types.
     * @param $order - order information
     */
    private function collectStatistics ( $order ) {
        foreach ( $order['line_items'] as $variantProduct ) {

            $product = $this->getProduct($variantProduct['product_id']);

            if ( $product == null || $this->title && $this->title != $variantProduct['name'] )
                continue;

            $type = $product['product_type'];

            $this->collectTitles( $variantProduct['name'] );
            $this->collectTypes( $type );

            $gross = $variantProduct['price'] * $variantProduct['quantity'];
            $net = $gross - $variantProduct['total_discount'];

            $this->statistics[$variantProduct['variant_id']] = [
                'name' => $variantProduct['name'],
                'type' => $type,
                'sku' => $variantProduct['sku'],
                'quantity' => $this->statistics[$variantProduct['variant_id']]['quantity'] + $variantProduct['quantity'],
                'gross' => $this->statistics[$variantProduct['variant_id']]['gross'] + $gross,
                'net' => $this->statistics[$variantProduct['variant_id']]['net'] +
                    ( $order['financial_status'] == 'paid' ? $net : -1 * $net ),
            ];
        }
    }

    /**
     *  The function collection all data.
     */
    public function collectData() {

        $this->categories['no-selected'] = ['title' => 'No selected', 'selected' => true];
        foreach ($this->collections as $collection)
            $this->collectCategories($collection);

        $this->types['no-selected'] = ['title' => 'No selected', 'selected' => true];
        $this->titles['no-selected'] = ['title' => 'No selected', 'selected' => true];
        foreach ($this->orders as $order) {
            $this->dates[] = $order['updated_at'];
            if( $this->title != null && count($this->statistics) )
                break;
            $this->collectStatistics($order);
        }

    }
}