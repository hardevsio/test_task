<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Slim\Views\PhpRenderer;
use \Slim\App;

use WorkWithStatistics\Statistic;


require_once '../vendor/autoload.php';
require_once 'lib/Statistic.php';

$app = new App(/*['settings' => [
    'displayErrorDetails' => true
]]*/);


$container = $app->getContainer();
$container['view'] = new PhpRenderer('./templates/');

$appLoad = function (Request $request, Response $response, $args) {

    $appStat = null;

    try{

        $appStat = new Statistic($args);
        $appStat->collectData();

    } catch (Exception $ex) {
        return $response->getBody()->write('[ Exception ] ' . $ex->getMessage() . '<br />');
    }


    return $this->view->render( $response, 'statistics.phtml', ['statistics' => json_encode($appStat->statistics), 'filter' => ['categories' => $appStat->categories, 'titles' => $appStat->titles, 'types' => $appStat->types, 'dates' => json_encode($appStat->dates) ] ] );
};

$app->get('/fromDate={min-date}&toDate={max-date}&cat={cat}&title={title}&type={type}', $appLoad);
$app->get('/', $appLoad);

$app->run();
