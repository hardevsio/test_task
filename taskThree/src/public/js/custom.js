'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MinMaxDate = function () {
    // constructor MinMaxDate init data of dates
    function MinMaxDate(dates, $fromDate, $toDate) {
        _classCallCheck(this, MinMaxDate);

        this.minDate = moment();
        this.maxDate = 0;
        this.searchDateOnMinAndMax(dates);
        this.turnOnFromDateInput($fromDate);
        this.turnOnToDateInput($toDate);
    }

    // search date min and max


    _createClass(MinMaxDate, [{
        key: 'searchDateOnMinAndMax',
        value: function searchDateOnMinAndMax(dates) {
            for (var i = 0; i < dates.length - 1; i++) {
                if (moment(dates[i]).valueOf() < moment(this.minDate).valueOf()) this.minDate = dates[i];
                if (moment(dates[i]).valueOf() > moment(this.maxDate).valueOf()) this.maxDate = dates[i];
            }
        }

        // install datetimepicker

    }, {
        key: 'turnOnFromDateInput',
        value: function turnOnFromDateInput($fromDate) {
            $fromDate.datetimepicker({
                value: moment(this.minDate).subtract(1, 'days').format('YYYY-MM-DD HH:mm'),
                format: 'yyyy-mm-dd HH:MM',
                uiLibrary: 'bootstrap4',
                icons: {}
            });
        }

        // install datetimepicker

    }, {
        key: 'turnOnToDateInput',
        value: function turnOnToDateInput($toDate) {
            $toDate.datetimepicker({
                value: moment(this.maxDate).add(1, 'days').format('YYYY-MM-DD HH:mm'),
                format: 'yyyy-mm-dd HH:MM',
                uiLibrary: 'bootstrap4',
                icons: {}
            });
        }
    }]);

    return MinMaxDate;
}();

var Statistic = function () {
    // constructor init data statistics on 1 page
    function Statistic(statistics, countPosts, $pagination, $statistics) {
        _classCallCheck(this, Statistic);

        var page = 1;
        this.countPosts = countPosts;
        this.countPages = Math.ceil(statistics.length / this.countPosts);

        this.fillPagination(page, $pagination);
        this.fillDataStatistics(page, statistics, $statistics);

        this.changePage(page, statistics, $pagination, $statistics);
    }

    // fill pagination


    _createClass(Statistic, [{
        key: 'fillPagination',
        value: function fillPagination(page, $pagination) {
            $('ul', $pagination).html('');
            for (var i = 1; i <= this.countPages; i++) {
                $('ul', $pagination).append($('<li class="page-item ' + (page == i ? 'disabled' : '') + '" />').append('<a class="page-link" href="#page' + i + '">' + i + '</a>'));
            }
        }

        // fill statistics

    }, {
        key: 'fillDataStatistics',
        value: function fillDataStatistics(page, statistics, $statistics) {
            $statistics.html('');
            for (var i = this.countPosts * page - this.countPosts; i < statistics.length; i++) {
                if (this.countPosts * page == i) break;
                $statistics.append($('<tr scope="row" />').append('<th>' + statistics[i].name + '</th>').append('<td>' + statistics[i].type + '</td>').append('<td>' + statistics[i].sku + '</td>').append('<td>' + statistics[i].quantity + '</td>').append('<td>' + statistics[i].gross + '</td>').append('<td>' + statistics[i].net + '</td>'));
            }
        }

        // change page

    }, {
        key: 'changePage',
        value: function changePage(page, statistics, $pagination, $statistics) {
            this.fillPagination(page, $pagination);
            this.fillDataStatistics(page, statistics, $statistics);
            var stat = this;
            $('a', $pagination).click(function (e) {
                e.preventDefault();
                stat.changePage(+$(this).text(), statistics, $pagination, $statistics);
            });
        }
    }]);

    return Statistic;
}();

$(function () {

    $("button[type=submit]").click(function () {
        $('img', this).show();
    });

    if (dates != undefined) {
        var minMaxDates = new MinMaxDate(dates, $('#fromDate'), $('#toDate'));
        $('#filter').submit(function (e) {
            e.preventDefault();
            window.location.href = window.location.origin + '/' + $(this).serialize();
        });
    }

    if (statistics != undefined) {
        statistics = Object.values(statistics);
        if (statistics.length) {
            var stat = new Statistic(statistics, 5, $('#pagination'), $('#statistics'));
        } else {
            $('#statistics').append($('<tr/>').append('<th colspan="5" class="text-center">Products not found!</th>'));
        }
    }
});