'use strict';
/*
* Обработка ошибок должна быть в каждой задаче
* */

var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var uglify = require('gulp-uglify');
var concatcss = require('gulp-concat-css');
//var minifyCss = require("gulp-minify-css");
var concat = require('gulp-concat');
var debug = require('gulp-debug');
var babel = require("gulp-babel");
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var jshint = require("gulp-jshint");
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');

// Configuration
var configuration = {
    paths: {
        src: {
            html: './src/*.html',
            css: ['./src/css/**/*.css'],

            sass: './src/scss/**/*.scss',
            sassoutput: './src/css/',
            img: './src/img/**/*.*',
            js: './src/js/**/*.js',
        },

        dist: './theme/assets',
        cssoutput: './style.css'
    }
};

// ========================== Image tasks =========================

gulp.task('img', function () {
    gulp.src(configuration.paths.src.img)
        .on('error', onError)/*обработка ошибок*/
        .pipe(gulp.dest(configuration.paths.dist + '/img'))
        .pipe(livereload());
});

// ========================== HTML  tasks =========================

gulp.task('html', function () {
    return gulp.src(configuration.paths.src.html)
        .on('error', onError)/*обработка ошибок*/
        .pipe(gulp.dest(configuration.paths.dist))

        .pipe(livereload());
});

// ========================== SCSS\CSS tasks =========================

gulp.task('sass', function () {
    return gulp.src(configuration.paths.src.sass)
        .pipe(debug({title: 'sass:'}))
        .pipe(sass())
        .on('error', onError)/*обработка ошибок*/
        .pipe(gulp.dest(configuration.paths.src.sassoutput))
});

gulp.task('concatcss', [], function () {
    console.log("Concating and moving all the css files in styles folder");
    gulp.src(configuration.paths.src.css)

        .pipe(concat('shopify-filter.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(configuration.paths.dist))
        .on('error', onError)/*обработка ошибок*/
        //.pipe(minifyCss({level: {1: {specialComments: false}}}))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename('shopify-filter.min.css'))
        .pipe(gulp.dest(configuration.paths.dist));
});

// ==========================  JS tasks =========================

gulp.task('js:main', function () {
       gulp.src(configuration.paths.src.js)
            .pipe(sourcemaps.init())
            .pipe(jshint())
            .pipe(jshint.reporter('jshint-stylish'))
            .pipe(concat("shopify-filter.js"))
            .pipe(babel({presets: ['env']}))
            .on('error', onError)
            .pipe(gulp.dest(configuration.paths.dist))
            .pipe(uglify())
            .pipe(rename('shopify-filter.min.js'))
            .pipe(sourcemaps.write('.'))

            .pipe(gulp.dest(configuration.paths.dist))
           .on('error', onError); /*обработка ошибок*/


    }
);

gulp.task('js:noconcat', function () {
    console.log("JS NoConcat");
    gulp.src(configuration.paths.src.js)
        .pipe(uglify())
        .on('error', onError)/*обработка ошибок*/
        .pipe(gulp.dest(configuration.paths.dist + '/js'));


});

// ========================== Watch =========================

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('src/**/*.scss', ['sass', /*'js',*//*'concatcss'*/]);
    gulp.watch('src/**/*.css', ['concatcss']);
    gulp.watch('src/js/*.js', ['js:main']);


});

gulp.task('default', ['watch']).on('error', onError);


function onError(err) {
    console.log(err);
    this.emit('end');
}