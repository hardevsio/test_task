'use strict';

$(function () {

  var filter = new Filter($('.FilterByCollections'), $('.FilterBySizes'), $('.FilterByVendors'));

  /*
  *	Click on Button Find on Filter. Event redirects.
  */
  $('#FilterByFind').click(function (e) {
    var href = '';
    if (filter.collections.val() != '')
      href = window.location.origin + '/collections/' + filter.collections.val();
    if (filter.sizes.val() != null && filter.sizes.val() != '' && filter.sizes.val() != 'all' || filter.vendors.val() != null && filter.vendors.val() != '' && filter.vendors.val() != 'all') 
      href += '/';
    if (filter.sizes.val() != null && filter.sizes.val() != '' && filter.sizes.val() != 'all') {
      href += filter.sizes.val();
      if (filter.vendors.val() != null && filter.vendors.val() != '' && filter.vendors.val() != 'all') 
        href += '+' + filter.vendors.val();
    } else if (filter.vendors.val() != null && filter.vendors.val() != '' && filter.vendors.val() != 'all')
		href += filter.vendors.val();
    window.location.href = href;
  });
});