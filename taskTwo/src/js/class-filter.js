'use strict';

class Filter {

	/*
	*	constructor filter
	*	cSelect - collections select on html ( jquery object )
	*	sSelect - sizes select on html ( jquery object )
	*	vSelect - vendors select on html ( jquery object )
	*/
    constructor(cSelect, sSelect, vSelect) {

        this.collections = cSelect;
        this.sizes = sSelect;
        this.vendors = vSelect;

        this.collections.change(this, this.changeCollections);
        this.collections.change();
    }
  
	/*
	*	event change collections select
	*	e - event data
	*/
    changeCollections(e) {
        var filter = e.data;
        var size = filter.sizes.val();
        filter.sizes.html('').append('<option value="" disabled selected>Size</option>');
        filter.addToSelect(filter.sizes, 'Size', size);
        var vendor = filter.vendors.val();
        filter.vendors.html('').append('<option value="" disabled selected>Brand</option>');
        filter.addToSelect(filter.vendors, 'Vendor', vendor);
    }

    /*
    *	check array twins
    *	el - getted element
    */
    checkArrayTwins(el){
        return (el.indexOf(this) < 0) == false;
    }

	/*
	*	add to select on html
	*	$select - jquery object for append tags
	*	prefixName - prefix tag name
	*	selected - selected tag
	*/
    addToSelect($select, prefixName, selected) {
        $select.append('<option value="all" ' + ( this.collections.val() == 'all' ? 'selected' : '' ) + ' >All</option>');
        var arrTags = [];
        var filter = this;
        collects.forEach(function(item) {
            if(item.handle == filter.collections.val() || filter.collections.val() == 'all')
            {
                item.products.forEach(function(item){
                    item.tags.forEach(function(item){
                        if(arrTags.find(filter.checkArrayTwins, item) == undefined && item.indexOf(prefixName) >= 0){
                            $select.append('<option value="' + item + '" ' + ( selected == item ? 'selected' : '' ) + ' >' + item.substring(prefixName.length + 1) + '</option>');
                            arrTags.push(item);
                        }
                    });
                });
            }
        });
    }

}