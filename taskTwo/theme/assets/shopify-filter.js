'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Filter = function () {

    /*
    *	constructor filter
    *	cSelect - collections select on html ( jquery object )
    *	sSelect - sizes select on html ( jquery object )
    *	vSelect - vendors select on html ( jquery object )
    */
    function Filter(cSelect, sSelect, vSelect) {
        _classCallCheck(this, Filter);

        this.collections = cSelect;
        this.sizes = sSelect;
        this.vendors = vSelect;

        this.collections.change(this, this.changeCollections);
        this.collections.change();
    }

    /*
    *	event change collections select
    *	e - event data
    */


    _createClass(Filter, [{
        key: 'changeCollections',
        value: function changeCollections(e) {
            var filter = e.data;
            var size = filter.sizes.val();
            filter.sizes.html('').append('<option value="" disabled selected>Size</option>');
            filter.addToSelect(filter.sizes, 'Size', size);
            var vendor = filter.vendors.val();
            filter.vendors.html('').append('<option value="" disabled selected>Brand</option>');
            filter.addToSelect(filter.vendors, 'Vendor', vendor);
        }

        /*
        *	check array twins
        *	el - getted element
        */

    }, {
        key: 'checkArrayTwins',
        value: function checkArrayTwins(el) {
            return el.indexOf(this) < 0 == false;
        }

        /*
        *	add to select on html
        *	$select - jquery object for append tags
        *	prefixName - prefix tag name
        *	selected - selected tag
        */

    }, {
        key: 'addToSelect',
        value: function addToSelect($select, prefixName, selected) {
            $select.append('<option value="all" ' + (this.collections.val() == 'all' ? 'selected' : '') + ' >All</option>');
            var arrTags = [];
            var filter = this;
            collects.forEach(function (item) {
                if (item.handle == filter.collections.val() || filter.collections.val() == 'all') {
                    item.products.forEach(function (item) {
                        item.tags.forEach(function (item) {
                            if (arrTags.find(filter.checkArrayTwins, item) == undefined && item.indexOf(prefixName) >= 0) {
                                $select.append('<option value="' + item + '" ' + (selected == item ? 'selected' : '') + ' >' + item.substring(prefixName.length + 1) + '</option>');
                                arrTags.push(item);
                            }
                        });
                    });
                }
            });
        }
    }]);

    return Filter;
}();

'use strict';

$(function () {

    var filter = new Filter($('.FilterByCollections'), $('.FilterBySizes'), $('.FilterByVendors'));

    /*
    *	Click on Button Find on Filter. Event redirects.
    */
    $('#FilterByFind').click(function (e) {
        var href = '';
        if (filter.collections.val() != '') href = window.location.origin + '/collections/' + filter.collections.val();
        if (filter.sizes.val() != null && filter.sizes.val() != '' && filter.sizes.val() != 'all' || filter.vendors.val() != null && filter.vendors.val() != '' && filter.vendors.val() != 'all') href += '/';
        if (filter.sizes.val() != null && filter.sizes.val() != '' && filter.sizes.val() != 'all') {
            href += filter.sizes.val();
            if (filter.vendors.val() != null && filter.vendors.val() != '' && filter.vendors.val() != 'all') href += '+' + filter.vendors.val();
        } else if (filter.vendors.val() != null && filter.vendors.val() != '' && filter.vendors.val() != 'all') href += filter.vendors.val();
        window.location.href = href;
    });
});