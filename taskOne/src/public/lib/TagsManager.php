<?php

namespace WorkWithTags;
use Dotenv\Dotenv;
use PHPShopify\PriceRule;
use PHPShopify\ShopifySDK;

class TagsManager {
	private $shopify = null; // Object ShopifySDK

	private $shopUrl = null; // URL shop

	private $products = []; // Data products

	private $collects = []; // Data collects

	private $discounts = []; // Data discounts
	
	public $logs = '';

	/**
	 * TagsManager constructor.
	 */
	public function __construct()
    {
        $dotenv = new Dotenv(__DIR__ . str_repeat(DIRECTORY_SEPARATOR . '..', 2));
        $dotenv->load();

        $this->shopUrl = $_ENV['SHOP'];

        $config = [
            'ShopUrl' => $_ENV['SHOP'],
            'ApiKey' => $_ENV['API_KEY'],
            'Password' => $_ENV['PASS'],
        ];

        $this->shopify = ShopifySDK::config($config);
        ShopifySDK::checkApiCallLimit();
        $this->initialize();
    }

    /**
    The function initialize this is object at make and after settings "ShopifySDK".
     */
	private function initialize() {
		$page = 1;
		$params = [
			'page' => $page,
			'default' => 250
		];
		do {
			$productsPage = $this->shopify->Product->get($params);
			if(!empty($productsPage))
				$this->products = array_merge($this->products, $productsPage);
			$params['page'] = ++$page;
		} while (!empty($productsPage));

		$page = 1;
		$params['page'] = $page;
		do {
			$collectsPage = $this->shopify->Collect->get($params);
			if(!empty($collectsPage))
				$this->collects = array_merge($this->collects, $collectsPage);
			$params['page'] = ++$page;
		} while (!empty($collectsPage));

		$priceRule = new PriceRule();
		$page = 1;
		$params['page'] = $page;
		do {
			$discountsPage = $priceRule->get($params);
			if(!empty($discountsPage))
				$this->discounts = array_merge($this->discounts, $discountsPage);
			$params['page'] = ++$page;
		} while (!empty($discountsPage));
	}


	/**
	 * The function getting products with collection.
	 * @param $prodId  - product ID
	 * @param $collectionsIdList - discount collections ID list
	 *
	 * @return bool
	 */
	private function isProductInDiscountCollections($prodId, $collectionsIdList) {
		foreach ($this->collects as $item)
			if ($item['product_id'] == $prodId && in_array($item['collection_id'], $collectionsIdList))
				return true;
		return false;
	}

    /**
     * The function checking variants product in variants id on existing.
     * @param $variantsIdList - discount variants ID list
     * @param array $variantsArray - variants array from product
     * @return bool - variants has discount in product
     */
	private function variantsHasDiscount($variantsIdList, array $variantsArray) {
		foreach ($variantsArray as $v)
			if (in_array($v['id'], $variantsIdList))
				return true;
		return false;
	}

    /**
     * The function return string for tag log.
     * @param $tag  - tag name
     * @param $product - product info
     * @return string - information string for log
     */
	private function makeLogStringAddTag($tag, $product){
	    return '<h3>Added tag "' . $tag . '" in <a href="https://' . $this->shopUrl . '/admin/products/' . $product['id'] . '">' . $product['title'] . '</a></h3>';
    }

    /**
     * The function add tag "Sale".
     * @param $tags - all tags in product, reference
     * @param $product - product info
     */
	private function addTagSale(&$tags, $product) {
		foreach ($this->discounts as $discount) {
			if ($discount['ends_at'] === null) {
				if (!in_array('Sale', $tags) && ($discount['target_selection'] == 'all' ||
					$discount['target_selection'] == 'entitled') &&
					(in_array($product['id'], $discount['entitled_product_ids'])
						|| $this->isProductInDiscountCollections($product['id'], $discount['entitled_collection_ids'])
						|| $this->variantsHasDiscount($discount['entitled_variant_ids'], $product['variants'])
					)
				) //
				{
					$tags[] = 'Sale';
                    $this->logs .= $this->makeLogStringAddTag('Sale', $product); // Log
				}

			}
		}
	}

    /**
     * The function add tag "Out of stock".
     * @param $tags - all tags in product, reference
     * @param $product - product info
     */
	private function addTagOutOfStock(&$tags, $product) {
		$quantity = 0;
		foreach ($product['variants'] as $item) {
			$quantity += $item['inventory_quantity'];
		}

		if ($quantity <= 0 && !in_array('Out of stock', $tags)) {
			$tags[] = 'Out of stock';
            $this->logs .= $this->makeLogStringAddTag('Out of stock', $product); // Log
		}

	}

    /**
     * The function adding tag to product.
     */
	public function addTagToProduct() {
		foreach ($this->products as $product) {
			$tags = $product['tags'] === '' ? [] : explode(', ', $product['tags']);

			$this->addTagSale($tags, $product);

			$this->addTagOutOfStock($tags, $product);

			$tags = implode(', ', $tags);
			if ($tags != $product['tags']) {
				$this->shopify->Product($product['id'])->put([
                    'id' => $product['id'],
                    'tags' => $tags,
                ]);
			}
		}
	}
}