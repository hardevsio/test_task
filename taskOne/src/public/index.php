<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use WorkWithTags\TagsManager;

require_once '../vendor/autoload.php';
require_once 'lib/TagsManager.php';
require_once 'lib/PriceRule.php';

$app = new \Slim\App(['settings' => [
    'displayErrorDetails' => true
]]);

$app->get('/', function (Request $request, Response $response) {

	$tagsManager = null;
    try{

        $tagsManager = new TagsManager();
	    $tagsManager->addTagToProduct();

    } catch (Exception $ex) {
	    return $response->getBody()->write('[ Exception ] ' . $ex->getMessage() . '<br />');
    }

    return $response->getBody()->write('<h2>[ Success ] - Task completed successfully!</h2><h1>Logs</h1>' . $tagsManager->logs);
});

$app->run();
